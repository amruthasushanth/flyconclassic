import { Injectable, TemplateRef } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { CredentialService } from './credential.service';
import 'rxjs/add/operator/map';
import { NbDialogService } from '@nebular/theme';


@Injectable()
export class CompanyService {
    token: any;
    userId: any;
    deleteId:number;
    update_id:any;
    constructor(private http: HttpClient, private credential: CredentialService,private dialogService: NbDialogService) {
      this.token = JSON.parse(localStorage.getItem('access_token')).token
      console.log("token ++++", this.token);
    }


    addCompany(id,data){
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Basic " + this.token);
      const uri = this.credential.local_dev_ip+"tenants/api/v1/company/" + id + "/stores"
      + data;
      console.log("service",data);
      const obj = {
        name:data.name,
        address:data.address,
        city:data.city,
        location:data.location,
        mobile:data.mobile,
      };
      console.log("Service addcompany",obj);
      return this.http.post(uri,obj);
    }


    getCompany(){
      console.log('inside service---GETlead');

      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Bearer" + this.token);
      // const uri = this.credential.dev_ip+'/oauth-server/oauth/token';
      const uri = this.credential.dev_ip+'/tenants/api/v1/company';
      console.log("company management URI",uri)
      return this
              .http
              .get(uri,{ headers: headers })
              .map(res => {
                console.log('uri----url', uri);
                console.log('res get service-------------------',res);
                return res;
              
                
              });
      
    }


    delete(id,store_id){
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Bearer" + this.token);
      console.log("inside delete------------",id,'store_id----------------',store_id);
      const uri = this.credential.dev_ip+"tenants/api/v1/company/" +
      id +
      "/stores/" +
      store_id
      ;
console.log('id-----',id,'store_id-----',store_id,'<<<<<<<<<<<<-----------uri---->>>',uri);

      return this.http.get(uri,{ headers: headers })
      .map(res=>{
        return res;
      });

    }
  

    edit(data){

      console.log(data);
      // const uri = this.credential.dev_ip+"tenants/api/v1/company/" + this.deleteId + "/stores/"+
      // data;
      this.update_id = data;

    }

    update(data){

      const uri = this.credential.dev_ip+"tenants/api/v1/company/" + this.update_id + "/stores/"+ data;
      console.log("inside companyservice--------------",this.update_id,data)

      return this.http.put(uri,data);


    }

    // deleteid(id){
    //   this.deleteId=id;
    // }

     
  }