import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { CredentialService } from './credential.service';
import 'rxjs/add/operator/map';


@Injectable()
export class LeadService {
    token: any;
    userId: any;
    constructor(private http: HttpClient, private credential: CredentialService) {
      this.token = JSON.parse(localStorage.getItem('access_token')).token
      console.log("token ++++", this.token);
    }


    addLead(leadForm){
      // const headers = new HttpHeaders()
      // .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      // .set("Authorization", "Basic " + this.token);
      const uri = this.credential.local_dev_ip+'/api/save';
      console.log("service",leadForm);
      const obj = {
        name:leadForm.name,
        mobile:leadForm.mobile,
        enquiry:leadForm.enquiry,
        expectedPurchaseDate:leadForm.expectedPurchaseDate,
      };
      console.log("Service addLead",obj);
      return this.http.post(uri,obj/*,{ headers: headers }*/)
      // .subscribe(res => console.log('Done addLead post'));
    }


    get_Lead(){
      console.log('inside service---GETlead');

      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Basic " + this.token);
      // const uri = this.credential.dev_ip+'/oauth-server/oauth/token';
      const uri = this.credential.local_dev_ip+'/api/getall';
      console.log("Lead management URI",uri)
      return this
              .http
              .get(uri,{ headers: headers })
              .map(res => {
                console.log('uri----url', uri);
                console.log('res get service-------------------',res);
                return res;
              
                
              });
      
    }


    view_lead(id){
      console.log('id--------');
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Basic " + this.token);
      const uri = this.credential.local_dev_ip+'/getone/'+id;
    
      return this
              .http
              .get(uri,{ headers: headers })
              .map(res => {
                console.log("res---------------",res);
               
                
                return res;
              });
    
    }

    // addLead(name,mobile,enquiry,expectedPurchaseDate) {
    //     console.log('testing addLead service---POST');
    //     const headers = new HttpHeaders()
    //       .set("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
    //       .set("Authorization", "Basic " + this.token);
    //       console.log('testing service inside---',enquiry);
    //     return this.http.post(
    //       this.credential.local_dev_ip+'/api/save',
    //       { 
    //         name: name, 
    //         mobile: mobile,
    //         enquiry:enquiry,
    //         expectedPurchaseDate:expectedPurchaseDate 
    //       },
    //       { headers: headers }
    //     )
        
    //   }

   

      // get_Lead(){
      //   // return this.http.get("localhost:8080/api/getall")
      //   // const uri = "localhost:8080/api/getall";
      //   // console.log("Lead management URI",uri)
      //   // return this
      //   //         .http
      //   //         .get(uri)
      //   const uri = this.credential.local_dev_ip+'/api/getall';
      //   console.log("Lead management URI",uri)
      //   return this
      //           .http
      //           .get(uri)
      //           .map(res => {
      //                         console.log('uri----url', uri);
      //                         console.log('res get service-------------------',res);
      //                         return res;
      //                       });
      // }

     
}