import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class CredentialService {
  result: any;
  flyCon_key='VVNFUl9DTElFTlRfQVBQOnBhc3N3b3Jk'
  dev_ip='http://api.gotobrainbox.com'
  local_dev_ip='http://localhost:8080' 
  loginApiurl: "http://api.gotobrainbox.com/oauth-server/oauth/token"
}
