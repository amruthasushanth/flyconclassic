import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { CredentialService } from './credential.service';
import 'rxjs/add/operator/map';
import { AuthenticationService } from '../../framework/auth';


@Injectable()
export class DashboardServices {
    token: any;
    userId: any;
    constructor(private http: HttpClient, private credential: CredentialService, private auth: AuthenticationService) {
        this.token = JSON.parse(localStorage.getItem('access_token')).token
        console.log("token ++++", this.token);
    }

    get_dashboardData() {
        console.log('inside service---get_dashboardData');
        const headers = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
        .set("Authorization", "Bearer " + this.token);
        const uri = this.credential.dev_ip+'/crm/api/v1/dashboard/data';
        console.log("Users URI",uri)
        return this
                .http
                .get(uri,{ headers: headers })
                .map(res => {
                  console.log('uri----url', uri);
                  console.log('res get service-------------------',res);
                  return res;
                
                  
                });

    }


    get_userName(){
        console.log('userName with id---');
        const headers = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
        .set("Authorization", "Bearer " + this.token);
        const uri = this.credential.dev_ip+'/tenants/api/v1/me';
        console.log("UsersNAME URI",uri)
        return this
                .http
                .get(uri,{ headers: headers })
                .map(res => {
                  console.log('uri----url', uri);
                  console.log('res get service--USERNAME-----------------',res);
                  return res;
                
                  
                });
        
    }



}