import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { CredentialService } from './credential.service';
import 'rxjs/add/operator/map';
import { AuthenticationService } from '../../framework/auth';


@Injectable()
export class FlyconUserService {
    token: any;
    userId: any;
    data:any;
    constructor(private http: HttpClient, private credential: CredentialService,
      private auth: AuthenticationService) {
      this.token = JSON.parse(localStorage.getItem('access_token')).token
      console.log("token ++++", this.token);
    }

    // data: any;


    addUser(data, store_id: number, company_id: number){
      // const headers = new HttpHeaders()
      // .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      // .set("Authorization", "Basic " + this.token);
      console.log('Add data services-----');
      
      const uri = this.credential.dev_ip+"tenants/api/v1/company/" +company_id + "/store/" +store_id +"/users"+data;
      console.log("service",data);
      const obj = {
        name:data.name,
        email:data.email,
        userName:data.userName,
        mobile:data.mobile,
        password:data.password,
        select_store:data.select_store,

      };
      console.log("Service users",obj);
      return this.http.post(uri,obj/*,{ headers: headers }*/)
      // .subscribe(res => console.log('Done addLead post'));
    }


    get_Users(){
      console.log('inside service---GETusers');

      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Bearer " + this.token);
      // const uri = this.credential.dev_ip+'/oauth-server/oauth/token';
      const uri = this.credential.dev_ip+'/tenants/api/v1/users';
      console.log("Users URI",uri)
      return this
              .http
              .get(uri,{ headers: headers })
              .map(res => {
                console.log('uri----url', uri);
                console.log('res get service-------------------',res);
                return res;
              
                
              });
      
    }


    
     
}