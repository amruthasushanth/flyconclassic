﻿export class Company {
  public name: string;
  public id: null;
  public description: string;
  public business: string;
  public tenantId: string;
  public address = new Address();
  public stores = new Store();
}
export class Address {
  public id: number = null;
  public city: any = "";
  public type: any = "";
  public text: any = "";
  public location = new Location();
}
export class Location {
  public id: number = null;
  public name: string = "";
}
export class Dashboard {
  public totalCustomers: any = 0;
  public activeCustomers: any = 0;
  public totalStores: any = 0;
  public totalUsers: any = 0;
  public loyaltyProgram: Array<loyaltyProgram> = [];
}
export class loyaltyProgram {
  public name: any = "";
  public status: any = "";
  public start: any = "";
  public end: any = "";
  public type: any = "";
}
export class Store {
  public name: string;
  public id: number;
  public description: string;
  public contactNumber: string = "";
  public image: string;
  public address = new Address();
  // public users = new Users();
  public users: Array<Users> = [];
  public socialAddresses: Array<SocialAddresses> = [];
}
export class SocialAddresses {
  public name: string = "";
  public socialId: string = "";
  public platform: string = "";
  public accessToken: string = "";
}
export class Users {
  public id: number = null;
  public name: string = "";
  public email: string = "";
  public mobile: number = null;
  public photo: any = "";
  public storeId: number = null;
  public address = new Address();
  public userName: string = "";
  public password: any = "";
  public confirmpassword: any = "";
  public roles: Array<SocialAddresses> = [];
}

export class Customers {
  public id: number;
  public name: string = "";
  public email: string = "";
  public gender: string = "";
  public maritalStatus: number = null;
  public anniversaryDate: any = "";
  public mobile: number = null;
  public dob: string = "";
  public ageGroup: number = null;
  public profession: any = "";
  public store: number = null;
  public address = new Address();
  public socialAddresses: Array<SocialAddresses> = [];
  public notificationSettings: Array<notificationSettings> = [];
}
export class notificationSettings {
  public optedInForCampaignEmails: boolean;
  public optedInForCampaignSms: boolean;
  public optedInForLoyaltyEmails: boolean;
  public optedInForLoyaltySms: boolean;
}

export class Group {
  public id: number = null;
  public code: any = null;
  public name: string = "";
  public type: string = "";
  public members: Array<Customers> = [];
  public createdOn: any = null;
}

export class List {
  public id: number = null;
  public code: any = null;
  public name: string = "";
  public type: string = "";
  public description: string = "";
  public members: Array<Customers> = [];
  public groups: Array<Group> = [];
}

export class Cloyalty {
  cId: any = "";
  points: string = "";
  amount: string = "";
  type: "";
  loyaltyotp: null;
}

export class Campaign {
  public id: number = null;
  public code: any = null;
  public title: string = "";
  public start_date: any = "";
  public end_date: any = "";
  public description: string = "";
  public members: Array<Customers> = [];
  public groups: Array<Group> = [];
  public lists: Array<List> = [];
  public activities: Array<Activity> = [];
}

export class Activity {
  public id: number = null;
  public type: string = "";
  public start_date: any = "";
  public end_date: any = "";
  public description: string = "";
  public channel: string = "";
  public channelSubject: string = "";
  public channelContent: string = "";
}

export class loyalty {
  public name: string = "";
  public status: string = "";
  public saleOutRule = new saleOutRule();
  public personalEventsRule = new personalEventsRule();
  public facebookActivitiRule = new facebookActivitiRule();
  public notificationRule = new notificationRule();
  public redemptionRule = new redemptionRule();
  public expiryRule = new expiryRule();
}

export class saleOutRule {
  amount: any = null;
  points: any = null;
}
export class personalEventsRule {
  birthdayPoints: any = null;
  anniversaryPoints: any = null;
}
export class facebookActivitiRule {
  pageLike: any = null;
  postLike: any = null;
  postShare: any = null;
  postComment: any = null;
}
export class notificationRule {
  sendPointAddedNotification: boolean = true;
  sendPointsExpiryReminders: boolean = true;
  sendPointsRedeemReminders: boolean = true;
}
export class redemptionRule {
  redemptionThreshold: any = null;
  points: any = null;
  amount: any = null;
}
export class expiryRule {
  enablePointsExpiry: boolean = true;
  expityType: boolean = true;
  expiryDate: any = null;
  exiryPeriod: any = null;
}

export class Common {
  public CONSTANTS = [{ id: "NOT_FOUND", message: "Search result not found" }];
}
