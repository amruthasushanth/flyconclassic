import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlyconLeadComponent } from './flycon-lead.component';

describe('FlyconLeadComponent', () => {
  let component: FlyconLeadComponent;
  let fixture: ComponentFixture<FlyconLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlyconLeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlyconLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
