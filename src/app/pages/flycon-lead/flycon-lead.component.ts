import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { LeadService } from '../../@core/data/lead.service';
import { DeleteLeadComponent } from '../delete-lead/delete-lead.component';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'flycon-lead',
  templateUrl: './flycon-lead.component.html',
  styleUrls: ['./flycon-lead.component.scss']
})
export class FlyconLeadComponent implements OnInit {

  
  p: number = 1;
searchText:[];
lead:any=[];


  constructor(private router:Router,private service:LeadService,private dialogService: NbDialogService ) { }

  ngOnInit() {
    this.getLead();
  }

  
getLead(){
  console.log('inside get Lead',this.lead);
  this.service.get_Lead().subscribe((res:any)=>{
    console.log('inside get Lead after subscribe--->res',res);
    this.lead=res;
    console.log('Lead array-res--',res);
    for(let i = 0, l= res.length; i<l ;i++){
      console.log('DaTA---res-',i);
    
          }
  })
}

  add_leads(){
    console.log('inside add_lead()--');
    this.router.navigate(['pages/add-lead']);
    
  }

  data(id){
    console.log('index---------',id);
    // this.service.view_lead()
    // var id = this.lead[0].id
    // console.log('id---------',id);
    // let leaData =this.lead;
    // for(var i = 0, l= leaData.length; i<l ;i++){
    //   // console.log('DaTA---res-',leaData[i].id);
    //   var item = [];
    //   item.push(leaData)
    //   console.log('item-----------',item);
    //       }
    // console.log('inside VIEW---',item);
    // let lead_data = [];
    // lead_data.push(item);
    // console.log('lead_data----------------------------------',lead_data);
    

    this.router.navigate(['pages/view/id']);
    
  }

  edit(id){
    console.log('inside edit with _id',id);
    this.router.navigate(['pages/edit-lead']);
    
  }

  open(dialog: TemplateRef<any>) {
    console.log('open delete box');
    this.dialogService.open(dialog, { context: 'Are you sure want to delete?' });
  }

  delete(id){
    console.log('inside delete----');
    
  }

}
