import { NgModule } from '@angular/core';
import { NbCardModule } from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
  imports: [
    NbCardModule,
    ThemeModule,
    RouterModule,
    NbEvaIconsModule,
    
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
