import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'People',
    icon: 'person-add-outline',
    children: [
      {
        title: 'Customers',
        link: '/pages/customers',
      },
      {
        title: 'Leads',
        link: '/pages/leads',
      },
    ],
  },

  {
    title: 'Integration',
    icon: 'clipboard-outline',
    children: [
      {
        title: 'Import customer',
        link: '/pages/import_customer',
      },
      {
        title: 'Import sales orders',
        link: '/pages/import_sales_orders',
      },
    ]
  },
  {
    title: 'Settings',
    icon: 'settings-2-outline',
    children: [
      {
        title: 'Company',
        link: '/pages/company',
      },
      {
        title: 'Users',
        link: '/pages/flycon_users',
      },
      // {
      //   title: 'Loyality and Rewards',
      //   link: '/pages/loyality/rewards',
      // },
    ]
  },

];
