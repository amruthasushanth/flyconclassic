import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LeadService } from '../../@core/data/lead.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'edit-lead',
  templateUrl: './edit-lead.component.html',
  styleUrls: ['./edit-lead.component.scss']
})
export class EditLeadComponent implements OnInit {
  editLeadForm: FormGroup;
  test:any=[];

  private index: number = 0;

  @HostBinding('class')
  classes = 'example-items-rows';

  constructor(private leadService:LeadService,private toastrService: NbToastrService) { 
    this.createForm();
  }
  ngOnInit() {
  }

  createForm() {
    this.editLeadForm = new FormGroup({
      name: new FormControl('',Validators.required),
      mobile: new FormControl('',[
        Validators.required,
        Validators.pattern("^[0-9]{10}"),
        Validators.minLength(8),
      ]),
      enquiry: new FormControl('',Validators.required),
      expectedPurchaseDate: new FormControl('',Validators.required)

    })

  }

}
