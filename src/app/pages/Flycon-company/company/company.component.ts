import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

import { NbDialogService } from '@nebular/theme';
import { Company } from '../../../@core/data/company';
import { CompanyService } from '../../../@core/data/company.service';


@Component({
  selector: 'company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  p: number = 1;
  searchText: [];
  company: any = [];
  head_company: any = [];
  delete_id: number;
  delete_storeid: number;
  store_id = [];
  new_company = new Company();
  update_id: any;


  constructor(private router: Router, private service: CompanyService, private dialogService: NbDialogService) {


    // this.getCompany();
    this.getMain();
  }

  ngOnInit() {
    this.getCompany();

  }

  getCompany() {
    console.log('inside get Lead', this.company);
    this.service.getCompany().subscribe((res: any) => {
      console.log('inside get Lead after subscribe--->res', res);
      this.company = res.stores;
      let store_id = []
      console.log('company array-res-users-', res.stores);
      this.company.forEach(element => {
        console.log('element',element);
        element.users.forEach(x => {
          console.log('x',x);
          store_id.push(x.storeId)
          // x.users.forEach(y => {
          //   console.log('x',x);
          //   store_id.push(y.storeId)
          // })
    console.log('store_id',store_id);

        })
      });


    })
  }

  getMain() {
    this.service.getCompany().subscribe((res: any) => {
      console.log('inside get Lead after subscribe--->res', res);
      this.head_company = res;

      console.log('headcompany array-res--', res.name);

    })


  }

  addCompany(id,data) {
    console.log('inside add_company()--');
    this.router.navigate(['pages/add-company']);
    this.service.addCompany(id,data).subscribe(res => {
      console.log('res--', res);
    })
  }

  edit(update_id) {
    console.log('inside edit with _id', update_id);
    this.router.navigate(['pages/edit-company']);
    this.update_id=update_id;
    this.service.edit(update_id);

  }


  open(dialog: TemplateRef<any>, id, store_id) {
    console.log('open delete box', id);
    this.dialogService.open(dialog, { context: 'Are you sure want to delete?' })
    this.delete_id = id;
    this.delete_storeid = store_id;
    console.log('VALUES inside DELETE------------------', id, store_id);

  }


  delete(id) {
    console.log('inside delete----1');

    this.service.delete(this.delete_id, this.delete_storeid).subscribe(res =>{
      console.log('----DELETE RES----',res);
      this.router.navigate(['pages/company'])
    });


  }





}
