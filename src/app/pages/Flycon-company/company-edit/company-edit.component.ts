import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanyService } from '../../../@core/data/company.service';


@Component({
  selector: 'company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss']
})
export class CompanyEditComponent implements OnInit {

  editcompanyForm: FormGroup;
  company: any;

  constructor(private companyService: CompanyService) { 
    this.createForm();
  }

  ngOnInit() {
  }
  createForm() {
    this.editcompanyForm = new FormGroup({
      name: new FormControl('',Validators.required),
      address: new FormControl('',Validators.required),
      city: new FormControl('',Validators.required),
      location: new FormControl('',Validators.required),
      mobile: new FormControl('',[
        Validators.required,
        Validators.pattern("^[0-9]{10}"),
        Validators.minLength(8),
      ]),

    })

  }


  update(data){
    console.log(data);
    this.companyService.update(data);

  }

  getCompany(){
    this.companyService.getCompany().subscribe((res:any)=>{
      console.log('inside get Lead after subscribe--->res',res);
      this.company=res.stores;
  })
}
}
