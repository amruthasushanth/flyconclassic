import { NgModule } from '@angular/core';
import { NbMenuModule, NbCardModule, NbButtonModule, NbInputModule, NbDatepickerModule, NbSidebarModule, NbSelectModule, NbIconModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { FlyconLeadComponent } from './flycon-lead/flycon-lead.component';
import { RouterModule } from '@angular/router';
import { AddLeadComponent } from './add-lead/add-lead.component';
import { EditLeadComponent } from './edit-lead/edit-lead.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FilterPipe } from './filter.pipe';
import { DeleteLeadComponent } from './delete-lead/delete-lead.component';
import { CredentialService } from '../@core/data/credential.service';
import { LeadService } from '../@core/data/lead.service';
import { LeadViewComponent } from './lead-view/lead-view.component';
import { FlyconUsersComponent } from './Users/flycon-users/flycon-users.component';
import { AddUsersComponent } from './Users/add-users/add-users.component';
import { CompanyComponent } from './Flycon-company/company/company.component';
import { CompanyAddComponent } from './Flycon-company/company-add/company-add.component';
import { CompanyEditComponent } from './Flycon-company/company-edit/company-edit.component';


@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NbDatepickerModule,
    NbSidebarModule,
    NbSelectModule,
    NbIconModule
  ],
  declarations: [
    PagesComponent,
    FlyconLeadComponent,
    CompanyComponent,
    CompanyAddComponent,
    CompanyEditComponent,
    AddLeadComponent,
    EditLeadComponent,
    FilterPipe,
    DeleteLeadComponent,
    LeadViewComponent,
    FlyconUsersComponent,
    AddUsersComponent
  ],
  entryComponents:[
    // DeleteLeadComponent
  ],
  providers: [
    // CredentialService,
    // LeadService, 
  ]
})
export class PagesModule {
}
