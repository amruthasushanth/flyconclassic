import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FlyconLeadComponent } from './flycon-lead/flycon-lead.component';
import { AddLeadComponent } from './add-lead/add-lead.component';
import { EditLeadComponent } from './edit-lead/edit-lead.component';
import { DeleteLeadComponent } from './delete-lead/delete-lead.component';
import { LeadViewComponent } from './lead-view/lead-view.component';
import { FlyconUsersComponent } from './Users/flycon-users/flycon-users.component';
import { AddUsersComponent } from './Users/add-users/add-users.component';
import { CompanyComponent } from './Flycon-company/company/company.component';
import { CompanyAddComponent } from './Flycon-company/company-add/company-add.component';
import { CompanyEditComponent } from './Flycon-company/company-edit/company-edit.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path:'leads',
      component:FlyconLeadComponent,
    },
    {
      path:'add-lead',
      component:AddLeadComponent,
    },
    {
      path:'edit-lead',
      component:EditLeadComponent,
    },

    {
      path:'company',
      component:CompanyComponent,
    },
    {
      path:'add-company',
      component:CompanyAddComponent,
    },
    {
      path:'edit-company',
      component:CompanyEditComponent,
    },
    {
      path:'view/id',
      component:LeadViewComponent,
    },
    {
      path:'flycon_users',
      component:FlyconUsersComponent,
    },
    {
      path:'add-users',
      component:AddUsersComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
