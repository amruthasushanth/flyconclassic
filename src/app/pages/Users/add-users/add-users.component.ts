import { Component, OnInit } from '@angular/core';
import { FlyconUserService } from '../../../@core/data/flycon-users.service';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Company, Store } from '../../../@core/data/company';

@Component({
  selector: 'add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss']
})
export class AddUsersComponent implements OnInit {

  usersForm: FormGroup;
  company = new Company();
  store = new Store();
  selectedItem = '2';
  store_id :any =[];
  users:any=[];

  constructor(private Service:FlyconUserService,private toastrService: NbToastrService,private router:Router) {
    this.createForm();
   }

  ngOnInit() {
    console.log('store_id--------',this.store_id);
    
  }

  createForm() {
    this.usersForm = new FormGroup({
      name: new FormControl('',Validators.required),
      mobile: new FormControl('',[
        Validators.required,
        Validators.pattern("^[0-9]{10}"),
        Validators.minLength(8),
      ]),
      email:new FormControl('',[Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      password: new FormControl('',Validators.required),
      confirm_password: new FormControl('',Validators.required),
      userName: new FormControl('',Validators.required),
      select_store: new FormControl('',Validators.required),
    })

  }

  add(data){
    console.log('inside leadForm---save--',this.usersForm.value);
    var company_id = this.company.id;
    var store_id = this.store.id;
    // this.Service.get_Users().subscribe((res:any)=>{
    //   console.log('inside get Users after subscribe--->res',res);
    //   this.users=res;
    //   console.log('Users array-res--',res);
    //   this.users.forEach(element => {
    //     this.store_id.push(element.storeId)
    //   });
    //   console.log('store_id-----',this.store_id);
  
    // })
    // console.log('store_id--addUser---',this.store_id);
    this.Service.addUser(data,company_id,store_id).subscribe(res=>{
      console.log('res--',res);
      this.toastrService.show(status, `Data saved successfully!`, { status:'success' });
      this.createForm();
      this.router.navigate(['pages/flycon_users']);

    },(err=>{
      console.log('Error--',err);
      this.toastrService.show(status, `Found Error`, { status:'danger' });

    }));
  }
  cancel(){
    this.router.navigate(['pages/flycon_users']);
  }

}
