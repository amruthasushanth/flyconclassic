import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlyconUsersComponent } from './flycon-users.component';

describe('FlyconUsersComponent', () => {
  let component: FlyconUsersComponent;
  let fixture: ComponentFixture<FlyconUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlyconUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlyconUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
