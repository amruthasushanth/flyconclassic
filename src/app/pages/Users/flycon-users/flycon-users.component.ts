import { Component, OnInit } from '@angular/core';
import { FlyconUserService } from '../../../@core/data/flycon-users.service';
import { Router } from '@angular/router';


@Component({
  selector: 'flycon-users',
  templateUrl: './flycon-users.component.html',
  styleUrls: ['./flycon-users.component.scss']
})
export class FlyconUsersComponent implements OnInit {

  p: number = 1;
  searchText:[];
  users:any=[
  //   {
  //   "name":"Amrutha",
  //   "store":"K.C",
  //   "username":"Ammu",
  //   "mobile":"9497899788"
  // },
  // {
  //   "name":"Devu",
  //   "store":"T.K.stores",
  //   "username":"deu",
  //   "mobile":"8497899788"
  // },
  // {
  //   "name":"Test",
  //   "store":"MP",
  //   "username":"asdf",
  //   "mobile":"8495899788"
  // }
];

 store_id :any =[];
  

  constructor(private service : FlyconUserService,private router:Router) { }

  ngOnInit() {
    this.getUser();
  }

   
getUser(){
  console.log('inside get Users',this.users);
  this.service.get_Users().subscribe((res:any)=>{
    console.log('inside get Users after subscribe--->res',res);
    this.users=res;
    console.log('Users array-res--',res);
    // this.users.forEach(element => {
    //   this.store_id.push(element.storeId)
    // });
    // console.log('store_id-----',this.store_id);

  })
}

add_users(){
  let store_id = [];
  console.log('inside USERS--add()--',this.users);
  this.users.forEach(element => {
      store_id.push(element.storeId)
    });
    console.log('store_id-----',store_id);

  this.router.navigate(['pages/add-users']);
  
}


}
