import { Component, OnInit } from '@angular/core';
import { LeadService } from '../../@core/data/lead.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'lead-view',
  templateUrl: './lead-view.component.html',
  styleUrls: ['./lead-view.component.scss']
})
export class LeadViewComponent implements OnInit {
  id:any;
  lead:any=[];
  data:any=[];

  constructor(private service:LeadService,private route:ActivatedRoute) { }

  ngOnInit() {
    console.log("this.id --------+",this.id);
    this.service.view_lead(this.id).subscribe(res=>{
      this.data=res;
    });

//     this.route.params.subscribe(params => {
//       this.service.view_lead(params['id']).subscribe(res => {
//          this.data = res;
//      });
//  });
  }

}
