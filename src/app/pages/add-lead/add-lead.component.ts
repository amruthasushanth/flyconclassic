import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LeadService } from '../../@core/data/lead.service';
import { NbToastrService, NbDateService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'add-lead',
  templateUrl: './add-lead.component.html',
  styleUrls: ['./add-lead.component.scss']
})
export class AddLeadComponent implements OnInit {
  leadForm: FormGroup;

  private index: number = 0;

  @HostBinding('class')
  classes = 'example-items-rows';

  constructor(private leadService:LeadService,private toastrService: NbToastrService,private router:Router) { 
    this.createForm();
  }

  ngOnInit() {
  }
  createForm() {
    this.leadForm = new FormGroup({
      name: new FormControl('',Validators.required),
      mobile: new FormControl('',[
        Validators.required,
        Validators.pattern("^[0-9]{10}"),
        Validators.minLength(8),
      ]),
      enquiry: new FormControl('',Validators.required),
      expectedPurchaseDate: new FormControl('',Validators.required)

    })

  }

  add(leadForm){
    console.log('inside leadForm---save--',this.leadForm.value);
    this.leadService.addLead(leadForm).subscribe(res=>{
      console.log('res--',res);
      this.toastrService.show(status, `Data saved successfully!`, { status:'success' });
      this.createForm();
      this.router.navigate(['pages/leads']);

    },(err=>{
      console.log('Error--',err);
      this.toastrService.show(status, `Found Error`, { status:'danger' });

    }));
  }
  reset(){
    this.createForm();
  }

}
