/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS, NbAuthSocialLink } from '../../auth.options';
import { getDeepFromObject } from '../../helpers';
import { NbAuthService } from '../../services/auth.service';
import { AuthenticationService } from '../../services/flycon_auth/authentication.service';
import { NbAuthResult } from '../../services/auth-result';
import { log } from 'util';

@Component({
  selector: 'nb-login',
  template: `
    <nb-auth-block>
    
      <h2 class="title"><img src="/assets/images/icon.png"/></h2>
     
      <small class="form-text sub-title">Hello! Sign in with your username or Email</small>

      <form (ngSubmit)="login()" #form="ngForm" autocomplete="nope">

        <div *ngIf="showMessages.error && errors && errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <!-- <div><strong>Oh snap!</strong></div> -->
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>

        <div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <!--<div><strong>Hooray!</strong></div>-->
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

        <div class="form-group">
          <label for="input-username" class="sr-only">Email address</label>
          <input name="username" [(ngModel)]="user.username" id="input-username" pattern=".+@.+\..+"
                 class="form-control" placeholder="Username" #username="ngModel"
                 [class.form-control-danger]="username.invalid && username.touched" autofocus
                 [required]="getConfigValue('forms.validation.username.required')">
          <small class="form-text error" *ngIf="username.invalid && username.touched && username.errors?.required">
            Username is required!
          </small>
          <small class="form-text error"
                 *ngIf="username.invalid && username.touched && username.errors?.pattern">
            username should be the real one!
          </small>
        </div>

        <div class="form-group">
          <label for="input-password" class="sr-only">Password</label>
          <input name="password" [(ngModel)]="user.password" type="password" id="input-password"
                 class="form-control" placeholder="Password" #password="ngModel"
                 [class.form-control-danger]="password.invalid && password.touched"
                 [required]="getConfigValue('forms.validation.password.required')"
                 [minlength]="getConfigValue('forms.validation.password.minLength')"
                 [maxlength]="getConfigValue('forms.validation.password.maxLength')">
          <small class="form-text error" *ngIf="password.invalid && password.touched && password.errors?.required">
            Password is required!
          </small>
          <small
            class="form-text error"
            *ngIf="password.invalid && password.touched && (password.errors?.minlength || password.errors?.maxlength)">
            Password should contains
            from {{ getConfigValue('forms.validation.password.minLength') }}
            to {{ getConfigValue('forms.validation.password.maxLength') }}
            characters
          </small>
        </div>

        <div class="form-group accept-group col-sm-12">
        <!-- <nb-checkbox name="rememberMe" [(ngModel)]="user.rememberMe">Remember me</nb-checkbox>
          <a class="forgot-password" routerLink="../request-password">Forgot Password?</a>-->
        </div>

        <button [disabled]="submitted || !form.valid" class="btn btn-block btn-info"
                [class.btn-pulse]="submitted">
          Sign In
        </button>
      </form>

      <div class="links">

        <ng-container *ngIf="socialLinks && socialLinks.length > 0">
          <small class="form-text">Or connect with:</small>

          <div class="socials">
            <ng-container *ngFor="let socialLink of socialLinks">
              <a *ngIf="socialLink.link"
                 [routerLink]="socialLink.link"
                 [attr.target]="socialLink.target"
                 [attr.class]="socialLink.icon"
                 [class.with-icon]="socialLink.icon">{{ socialLink.title }}</a>
              <a *ngIf="socialLink.url"
                 [attr.href]="socialLink.url"
                 [attr.target]="socialLink.target"
                 [attr.class]="socialLink.icon"
                 [class.with-icon]="socialLink.icon">{{ socialLink.title }}</a>
            </ng-container>
          </div>
        </ng-container>

        <small class="form-text sub-title">By signing in, you agree to the Terms & Conditions and Privacy Policy</small>

       <!-- <small class="form-text">
          Don't have an account? <a routerLink="../register"><strong>Sign Up</strong></a>
        </small>-->
      </div>
    </nb-auth-block>
  `,
})
export class NbLoginComponent {
  redirectDelay: number = 2000;
  showMessages: any = {};
  strategy: string = '';
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;
  socialLinks: NbAuthSocialLink[] = [];
  constructor(protected service: NbAuthService, protected flyAuth: AuthenticationService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected router: Router) {
    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.strategy = this.getConfigValue('forms.login.strategy');
    this.socialLinks = this.getConfigValue('forms.login.socialLinks');
    var token = JSON.parse(localStorage.getItem("token"));
    // let username = JSON.parse(localStorage.getItem("username"));
    // if(localStorage.getItem('auth_token')){
    //   this.router.navigateByUrl('pages/dashboard');
    // }else{
    //   localStorage.removeItem("username");
    // }

  }
  
  login(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    console.log('result---user',this.user);
    this.flyAuth.login(this.user.username, this.user.password).subscribe((result: any) => {
      this.submitted = false;
      let res = Object.values(result);
      console.log('result---log',res);
     
      if(result) {
        this.messages = ['You are successfully logged in..!'];
        console.log('Successfully logged In');
        localStorage.setItem('access_token', JSON.stringify({ token: res[0] }));
        this.router.navigateByUrl('pages/dashboard');
    //     this.token = JSON.parse(localStorage.getItem('access_token')).token
    console.log("token ++++", JSON.parse(localStorage.getItem('access_token')).token);
        // this.router.navigateByUrl('pages/dashboard');
        // this.route.parent.component
      }else{
        console.log('Not');
        this.submitted = false;
        this.showMessages = {     // show/not show success/error messages
          success: false,
          error: true,
        }
        this.errors = [result.message];
        
      }
      
      
      // if(result.success==true && result.role==true){
      //   this.messages = ['You are successfully logged in..!'];
      //   this.showMessages = {     // show/not show success/error messages
      //     success: true,
      //     error: false,
      //   }
        
      //   localStorage.setItem('auth_token', result.token);
      //   localStorage.setItem('username',result.username); //not recomended after remove 
         
      //   setTimeout(() => {
      //     console.log("timout called");
      //       this.router.navigateByUrl('pages/dashboard');
      //       console.log('this.user',this.user);
      //       localStorage.setItem("username", JSON.stringify(this.user.username));
      //     }, 1000);
      //   }
        
        
      //   else if(result.success==true && result.role==false){
      //     // this.router.navigateByUrl('pages/dashboard');
      //     localStorage.setItem("username", JSON.stringify(this.user.username));
      //   }
       
      // else{
      //   this.submitted = false;
      //   // console.log("error",err);
      //   this.showMessages = {     // show/not show success/error messages
      //     success: false,
      //     error: true,
      //   }
      //   this.errors = [result.message];
      // }
      console.log("response====================>", result);

    }, 
    err => {
      console.log("Invalid",err)
      this.submitted = false;
    });
    
  }
  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
