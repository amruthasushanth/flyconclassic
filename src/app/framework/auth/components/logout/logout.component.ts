/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { NB_AUTH_OPTIONS } from "../../auth.options";
import { getDeepFromObject } from "../../helpers";
import { NbAuthService } from "../../services/auth.service";
import { NbAuthResult } from "../../services/auth-result";
import { AuthGuard } from "../../services/flycon_auth/auth.guard";
import { AuthenticationService } from "../../services";

@Component({
  selector: "nb-logout",
  template: `
    <div>Logging out, please wait...</div>
  `
})
export class NbLogoutComponent implements OnInit {
  redirectDelay: number = 1000;
  strategy: string = "";

  constructor(
    protected service: NbAuthService,
    @Inject(NB_AUTH_OPTIONS) protected options = {},
    private flyAuth: AuthenticationService,
    protected router: Router
  ) {
    this.redirectDelay = this.getConfigValue("forms.logout.redirectDelay");
    this.strategy = this.getConfigValue("forms.logout.strategy");
  }

  ngOnInit(): void {
    this.logout();
  }

  logout(): void {
    console.log("result in logout");
    this.flyAuth.logout();
    setTimeout(() => {
      return this.router.navigateByUrl("auth/login");
    }, this.redirectDelay);
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
