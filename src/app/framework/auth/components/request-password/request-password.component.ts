/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS } from '../../auth.options';
import { getDeepFromObject } from '../../helpers';

import { NbAuthService } from '../../services/auth.service';
import { NbAuthResult } from '../../services/auth-result';
import { AuthenticationService } from '../../services';

@Component({
  selector: 'nb-request-password-page',
  styleUrls: ['./request-password.component.scss'],
  template: `
    <nb-auth-block>
      <h2 class="title" style="text-align:center;">Forgot Password</h2>
      <small class="form-text sub-title">Enter your email adress and we’ll send an OTP to reset your password</small>
      <form (ngSubmit)="requestPass()" #requestPassForm="ngForm">

        <div *ngIf="showMessages.error && errors && errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Oh snap!</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>
        <div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Hooray!</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

        <div class="form-group">
          <label for="input-email" class="sr-only">Enter your email address</label>
          <input name="email" [(ngModel)]="user.email" id="input-email" #email="ngModel"
                 class="form-control" placeholder="Email address" pattern=".+@.+\..+"
                 [class.form-control-danger]="email.invalid && email.touched"
                 [required]="getConfigValue('forms.validation.email.required')"
                 autofocus>
          <small class="form-text error" *ngIf="email.invalid && email.touched && email.errors?.required">
            Email is required!
          </small>
          <small class="form-text error"
                 *ngIf="email.invalid && email.touched && email.errors?.pattern">
            Email should be the real one!
          </small>
        </div>

        <button [disabled]="submitted || !requestPassForm.form.valid" class="btn btn-hero-success btn-block"
                [class.btn-pulse]="submitted">
          Request password
        </button>
      </form>

      <div class="links col-sm-12">
        <small class="form-text">
          Already have an account? <a routerLink="../login"><strong>Sign In</strong></a>
        </small>
        <small class="form-text">
          <a routerLink="../register"><strong>Sign Up</strong></a>
        </small>
      </div>
    </nb-auth-block>
  `,
})
export class NbRequestPasswordComponent {

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS) protected options = {},
              protected router: Router,protected flyAuth: AuthenticationService) {

                ////local storage//
    var token = JSON.parse(localStorage.getItem("token"));
    //console.log("dashboard-token------------------>",token);
    // let email = JSON.parse(localStorage.getItem("email"));
    let email = JSON.parse(localStorage.getItem(""));
    
     //console.log("email--------------------",email);

    this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
    this.strategy = this.getConfigValue('forms.requestPassword.strategy');
  }

  requestPass(): void {
    this.errors = this.messages = [];
    this.submitted = true;

    this.flyAuth.forGot(this.user.email).subscribe((result:any)=>{
      this.submitted=false;
      if(result.success==true){
        this.messages = ['Successfully requested password. '];
        this.showMessages = {     // show/not show success/error messages
          success: true,
          error: false,
      }
      //console.log('result----------------------------------',result);
        
        localStorage.setItem('auth_token', JSON.stringify(result));
        setTimeout(() => {
          //console.log("timout called");
            this.router.navigateByUrl('auth/otp');
            //console.log('this.user',this.user);
            
            localStorage.setItem("email", JSON.stringify(this.user.email));
           
          }, 1000);
        }
        else{
          this.submitted = false;
          // console.log("error",err);
          this.showMessages = {     // show/not show success/error messages
            success: false,
            error: true,
          }
          this.errors = ['Invalid Credentials..!'];
        }
        //console.log("response====================>", result);
  
      }, (err: any) => {
       
      
    })
  }

    // this.service.requestPassword(this.strategy, this.user).subscribe((result: NbAuthResult) => {
    //   this.submitted = false;
    //   if (result.isSuccess()) {
    //     this.messages = result.getMessages();
    //   } else {
    //     this.errors = result.getErrors();
    //   }

    //   const redirect = result.getRedirect();
    //   if (redirect) {
    //     setTimeout(() => {
    //       return this.router.navigateByUrl(redirect);
    //     }, this.redirectDelay);
    //   }
    // });
  

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
