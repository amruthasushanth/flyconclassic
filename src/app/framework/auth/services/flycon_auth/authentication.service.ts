import { Injectable } from "@angular/core";
import { HttpHeaders, HttpParams, HttpClient } from "@angular/common/http";
import { CredentialService } from "../../../../@core/data/credential.service";
import { Router } from "@angular/router";

@Injectable()
export class AuthenticationService {
  redirectDelay: number = 0;
  showMessages: any = {};
  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  token: any;
  constructor(
    private _http: HttpClient,
    private credential: CredentialService,
    protected router: Router
  ) {}


  login(username: string, password: string) {
    console.log('testing service');
    const headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")

      .set("Authorization", "Basic " + this.credential.flyCon_key);
      console.log('testing service inside---',username);
    return this._http.post(
      this.credential.dev_ip + "/oauth-server/oauth/token?username="+ username +"&password="+password+"&grant_type=password",
      { username: username, password: password },
      { headers: headers }
    )
    
  }
 
  ////logout///
  logout() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("username");
    // return this.router.navigateByUrl('auth/login');
  }


////////////REQUEST PASSWORD////
forGot(email){
  const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set("Authorization", "Bearer " + this.token);
const uri = this.credential.dev_ip + '/channel.Kloo.com/digital/api/v1/forGotPwd/?clientId=kloo20170012018';
const obj = {
 
  email: email,
 
 
};
//console.log("test frgt pwd", obj);
 return this
 ._http.post(uri, obj, { headers: headers });
  
}


////OTP///
otpVerify(email,otp){
  const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set("Authorization", "Bearer " + this.token);
const uri = this.credential.dev_ip + '/channel.Kloo.com/digital/api/v1/otpvalid/?clientId=kloo20170012018';
const obj = {
  email:email,
  otp: otp,
 
 
};
//console.log("test frgt pwd", obj);
 return this
 ._http.post(uri, obj, { headers: headers });
  
}

////CHANGE PASSWORD///\
changePassword(email,otp,password){
  const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set("Authorization", "Bearer " + this.token);
const uri = this.credential.dev_ip + '/api';
const obj = {
  email:email,
  otp:otp,
  password:password,
 
 
};
//console.log("test frgt pwd", obj);
 return this
 ._http.post(uri, obj, { headers: headers });
  
}

getProfile(email){
  console.log('id service-------------------',email);
  const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set("Authorization", "Bearer " + this.token);
  const uri = this.credential.dev_ip+'/api';
  console.log("uri--------------------->>",uri)
  return this
          ._http
          .get(uri,{ headers: headers })
          .map(res => {
            console.log('res service-------------------',res);
            return res;
          
            
          });
}



reSetp (email,password) {

  const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set("Authorization", "Bearer " + this.token);
  const uri = this.credential.dev_ip + '/api';
  const obj = {
  email:email,
    password: password,
   
  };
  console.log("test userPermission", obj);
   return this
   ._http.post(uri, obj, { headers: headers });
    
 }



updateProfile(firstName,lastName,mobile,password,id){
  const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set("Authorization", "Bearer " + this.token);
const uri = this.credential.dev_ip + '/api';
const obj = {
  firstName: firstName,
  lastName: lastName,
  mobile: mobile,
  // email: email,
  password: password,
};
console.log("test PROFILE page",uri, obj);
 return this
 ._http.post(uri, obj, { headers: headers });
  
}





  getRefreshToken() {
    let tk = JSON.parse(localStorage.getItem("LoggedInUser"));
     return tk.refresh_token;
  }


  checkToken(){

    let expiry = JSON.parse(localStorage.getItem("expiry"));
 
      if ( expiry <= Date.now() ) {
       
           if( this.refreshToken() !== null) {
               return this.refreshToken();
            } else {
             this.logout();
            }
 
     } else {
      console.log(this.displayTime(expiry)+ ' > '+ this.displayTime(Date.now()));
      return this.getToken();
     }
 
   }
   displayTime(value) { 
    var date = new Date(parseInt(value)); //datetime in IST
    return date.toLocaleTimeString() ;
  }

  getToken() {
    let tk = JSON.parse(localStorage.getItem("LoggedInUser"));
    if (tk) {
      if (tk.access_token) {
        return tk.access_token;
      } else {
        return null
      }
    }
    else { return null }

  }

  sendToken(token) {
    localStorage.setItem("LoggedInUser", token);
    this.router.navigate([ '/' ]);
  }


  refreshToken()   {
    
    let refresh_token =  this.getRefreshToken();
    let params = new URLSearchParams();
    params.append("grant_type", "refresh_token");   
    params.append("refresh_token", refresh_token);
    const headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")

      .set("Authorization", "Basic " + this.credential.flyCon_key);
     
    this._http.post(this.credential.loginApiurl, params.toString(),  { headers: headers })
    console.log('TESTED Refresh ++++++++++ TOKEN');
    
      // .subscribe(
      //   response => { 
      //     let user = response.json();
      //   if (user && user.access_token) {   
      //             let expireDate = new Date().getTime() + (1000 * user.expires_in);            
      //            localStorage.setItem("expiry", JSON.stringify(expireDate));       
      //            localStorage.setItem("LoggedInUser", JSON.stringify(user));  
      //          console.log(user)
      //          return user.access_token;
      //     } else {
      //       return null;
      //     }
           
      // },
      // err => {
      //   this.logout();
      // }); 
  
   }

 
}

