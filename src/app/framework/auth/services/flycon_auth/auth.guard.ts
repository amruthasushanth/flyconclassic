import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { NbAuthService } from "../auth.service";
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private _authService: NbAuthService) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem("access_token") != null) {
      return true;
    }
    console.log('token valiation top section');
    this.router.navigate(["auth/login"], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
